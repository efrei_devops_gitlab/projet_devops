variable "instance_type" { 
  default = "t2.micro"
}

########################
# Instances
########################
data "aws_ami" "web_ami" { 
  most_recent = true
  owners = ["self"] 
  name_regex = "^WebApp"
}

# ASG Launch Configuration
resource "aws_launch_configuration" "web" {
  image_id = data.aws_ami.web_ami.id
  instance_type = var.instance_type
  iam_instance_profile = "LabInstanceProfile"
  security_groups = [aws_security_group.private.id]
  lifecycle {
    create_before_destroy = true
  }
}
# Load Balancer
resource "aws_lb" "web" {
  name = "web"
  internal = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.private.id]
  subnets = [for subnet in aws_subnet.public_subnets : subnet.id]
  enable_deletion_protection = false
}
resource "aws_lb_target_group" "web" {
  name = "web"
  port = 8080
  protocol = "HTTP"
  vpc_id = aws_vpc.web.id
  health_check {
    enabled = true
    healthy_threshold = 3
    interval = 15
    path = "/"
    port = 8080
    timeout = 5
    unhealthy_threshold = 3
  }
}
resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.web.arn
  port = 8080
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.web.arn
  }
}
resource "aws_autoscaling_group" "web" {
  max_size = 4
  min_size = 2
  target_group_arns = [aws_lb_target_group.web.arn]
  launch_configuration = aws_launch_configuration.web.name
  vpc_zone_identifier = [for subnet in aws_subnet.private_subnets : subnet.id]
  health_check_type = "ELB"
  health_check_grace_period = 300
  force_delete = true
}
resource "aws_autoscaling_policy" "web" {
  name                   = "web-policy"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_cloudwatch_metric_alarm" "web" {
  alarm_name          = "web-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 2
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = 120
  statistic           = "Average"
  threshold           = 80

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.web.arn]
}
# DNS name of the ELB (wait for a few minutes before it becoming accessible on first deployment)
output "lb_dns_name" {
  description = "The DNS name of the ELB"
  value = aws_lb.web.dns_name
}